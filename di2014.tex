\documentclass{llncs}

\usepackage{graphicx}
\usepackage{subfig}
\usepackage{url}
\usepackage[utf8]{inputenc}

\graphicspath{{media/}}

\begin{document}

\title{Poppy Project: Open-Source Fabrication \\ of 3D Printed Humanoid Robot \\ for Science, Education and Art}

\titlerunning{Poppy project}
\author{Matthieu Lapeyre\inst{1} \and Pierre Rouanet\inst{1} \and Jonathan Grizou\inst{1} \and Steve Nguyen\inst{1} \and Fabien Depraetre\inst{1} \and Alexandre Le Falher\inst{1} \and Pierre-Yves Oudeyer\inst{1}}

\authorrunning{M.Lapeyre and al.} % abbreviated author list (for running head)
%
%%%% list of authors for the TOC (use if author list has to be modified)
% \tocauthor{Ivar Ekeland, Roger Temam, Jeffrey Dean, David Grove,
% Craig Chambers, Kim B. Bruce, and Elisa Bertino}
%
\institute{
Flowers Lab\inst{1}, Inria, France\\
contact: \email{matthieu.lapeyre@inria.fr},\\
Lab page: \url{https://flowers.inria.fr} \\
Project page: \url{www.poppy-project.org}
}

\maketitle

\abstract{Poppy is the first complete open-source 3D printed humanoid platform. Robust and accessible, it allows scientists, students, geeks, engineers or artists to explore fast and easily the fabrication and programming of various robotic morphologies. Both hardware and software are open-source, and a web platform allows interdisciplinary contributions, sharing and collaborations.}

% However, such a scientific challenge and exploring it with experiment in the real world, is a major and fundamental methodological problem: it requires to consider the body as an experimental variable, i.e. tuning freely and easily parameters of the body in order to study their impact on behavior.
% The emergence of 3D printing and rapid prototyping techniques has opened up new opportunities that the Flowers Lab caught. Thus we have developed a methodology and a humanoid robotic platform using 3D printing, and making it possible for both the exploration of body shapes previously very difficult to construction and establishment of a design cycle for rapid iterations on the morphological design. Very quickly, it was decided that this platform would be made open-source, so that scientific results are reproducible and reusable in other laboratories, but also to allow other laboratories to use the platform for themselves their research by re-using the results already achieved. [This project was concretized with the Poppy robot and the Poppy project which led the overall vision of promoting open innovation through the use of open source solution in combinaison with modern web-based community tools and development management.
% To make experiments on the body than any other platform currently allows low cost (about 7000 euros) also opens a laboratory versatile and affordable platform for research on many other subjects.


\section{The Poppy Project} % (fold)
\label{sec:the_poppy_project}

The Poppy Project develops an open-source 3D printed humanoid platform based on robust, flexible, easy-to-use and reproduce hardware and software. In particular, the use of 3D printing and rapid prototyping technologies is a central aspect of this project, and makes it easy and fast not only to reproduce the platform, but also to explore morphological variants. Poppy targets three domains of use: science, education and art.

Poppy was initially designed with a scientific objective, aiming to be a new experimental platform opening the possibility to systematically study the role of morphology in sensorimotor control, in human-robot interaction and in cognitive development. Indeed, a suitable design of a robot morphology can greatly simplify control problems, increase robustness, and open new modes of interaction with the physical and social world. Thus, being able to study the body as an experimental variable, something which can be systematically changed and experimented, is of paramount importance. Yet, until recently it was complicated because building a robot relied on heavy and costly manufacturing techniques. 3D printing has changed the landscape of what is possible: Poppy Project transposed it to humanoid robotics, and it is now possible to explore new body shapes in just a few days. It enables and simplifies the experimentation, the reproduction and the modification of the morphology in research laboratories. It also allows collaborative working, sharing and replication of the results on these issues between laboratories. The ambition is to become a reference platform for benchmarking and dissemination of scientific results.

Thanks to the fact that it integrates advanced and yet easily accessible techniques in an embodiment that motivates students and the wider public, this platform also meets a growing societal need: education and training in technologies combining computer science, electronics and mechanics, as well as a training tool to the emergent revolutionary 3D printing process. With its openness, its design and its rather low-cost, Poppy provides a unique context for experimentation and learning of these technologies in a Do-It-Yourself (DIY) approach. Several experiences with Poppy in secondary, high schools, science museums and Fablabs in France and abroad are underway and will be discussed in the incoming sections. Finally, the possibility to easily modify both the hardware and the software also makes Poppy a useful tool for artistic projects working with interactive computerized installations.

\subsection{Open-Source Robotic Platform} % (fold)

Poppy (Fig.~\ref{fig:poppy_with_bended_legs}) is the first complete 3D printed open-source and open-hardware humanoid robot. Its 3D printed skeleton and its Arduino-based electronics are open-hardware (Creative Commons). Its software is open-source (GPL V3), and allows programming beginners as well as advanced roboticists to control the robot in Python thanks to the PyPot library (\url{www.poppy-project.org/pypot-library/}). Its motors are common off-the-shell Robotis actuators (\url{http://www.robotis.com/xe/dynamixel_en}), and allow for compliant control and soft physical human-robot interaction. Poppy presents an original mechanical structure which permits to obtain a light structure with 3.5kg for 84cm height. Before the arrival of 3D printing techniques, this kind of complex structure was either impossible to produce or extremely expensive. Now, anyone can produce and modify such robot in their home using affordable personal 3D printers.

Several web tools support collaboration and sharing among members of the Poppy community: a portal web site (\url{www.poppy-project.org}), GitHub repositories for the hardware and software with associated wikis for documentation (\url{www.github.com/poppy-project/}), and a forum based on Discourse\footnote{\url{www.discourse.org}} technology (\url{forum.poppy-project.org}).

\begin{figure}[!t]
\centering
    \subfloat[][bended thighs]{\label{fig:poppy_with_bended_legs}\includegraphics[width=0.25\linewidth]{poppy_bended_tigh_square.pdf}}
    \hfil
    \subfloat[][straight thighs]{\label{fig:poppy_with_classical_legs}\includegraphics[width=0.25\linewidth]{poppy_straight_tigh_square.pdf}}
    \caption{3D printing allows very fast design/print/experiment cycles. Here, we used it to compare the impact of two thigh morphologies on biped balance control: (a) thigh bended by an angle of 6\textsuperscript{o} and (b) a more classical approach with straight thighs.}
    \label{fig:poppy_compared}
\end{figure}

\section{Case Studies} % (fold)

\textbf{Science: Studying the Role of Morphology in Biped Locomotion} The geometry and distribution of mass in the body has complex influences on biped locomotion. Thanks to the conception of Poppy allowing easy, cheap and fast morphology modifications, we were able to experiment the impact of various thigh shapes on the robot dynamics. In particular, we have investigated in \cite{lapeyre:hal-00861110}  the impact of a bio-inspired thigh, bended of 6\textsuperscript{o} and with a geometry inspired by humans, on the balance and biped locomotion and we have compared this design with a more traditional straight thigh (see Fig.~\ref{fig:poppy_compared}). A few robots like HRP-4C~\cite{kaneko2009cybernetic} and Kenshiro humanoid~\cite{nakanishi2013design} robots seem to visually have a morphology design close to the thigh shape of Poppy, but no comparative study of the role of this shape was presented so far.

Experiments conducted with Poppy have shown that the bio-inspired thigh allows the reduction of falling speed by almost 60\% (single support phase) and the decrease of the lateral motion needed for the mass transfer from one foot to the other by 30\% (double support phase). We also conducted an experiment where the robot produces a dynamic gate on a treadmill and we have measured that the bended thigh reduces the upper body motion by about 45\% indicating a more stable gait \cite{lapeyre:hal-00861110}. Walking happens currently with external support, since the robot is not equipped with balancing algorithms. The design of such a capability is the object of several ongoing projects in the community, where the forum we designed is used to openly share scientific ideas (see \url{https://forum.poppy-project.org/category/science}). \\

\textbf{Education: Learning to Build Poppy in a Science Museum} On march 22th \& 23th 2014, UniverSciences\footnote{Paris museum of sciences and technologies} organized a hackathon for the general public around the assembly of a Poppy robot (see \figurename~\ref{fig:poppy_universience}). It involved 15 robotic enthusiasts, from children to adults. Participants were dispatched around several workshops during the two days. While a group was dedicated to the actual assembly of the different Poppy parts, others were exploring how to program the robot with the Python software or working on designing and 3D printing hardware improvements.  Aside the workshops around Poppy, several presentations and conferences about robotics were set-up. In this context, participants are not only spectators of a scientific mediation act but also actors.

In two days, this group of new users, self-trained using online documentation have been able to build from scratch the whole robot and make it move using the Pypot library. They even designed a new original semi-passive solution for the ankle joint, as well as a robot helmet which was 3D printed and assembled within the time of the workshop. This experiment did not only show that the platform was easily usable in an educational context with users of all ages, and was rebuildable in two days by a little group, but it also showed high educational value as testified by users and educators (see \url{https://forum.poppy-project.org/t/poppy-project-at-la-cite-des-sciences-et-de-lindustrie/})

\begin{figure}[]
\centering
    \subfloat[][Universcience hackathon in progress]{\includegraphics[height=5cm]{hackathon.jpg}}
    \hfil
    \subfloat[][Poppy fully-assembled]{\includegraphics[height=5cm]{poppy_universcience.jpg}}
    \caption{The Poppy humanoid platform was entirely reproduced in a 2-day educational workshop by visitors of Paris science museum.}
    \label{fig:poppy_universience}
\end{figure}

\textbf{Art: Movements with a Dancer} The artist community is a rich source of inspiration and can provide new perspectives to scientific and technological questions. This complementarity is a great opportunity that we want to enforce in the Poppy project by making the robot accessible to non-robotic-expert users. The first experimentation of the use of Poppy in an art project was an artist residency entitled "Êtres et Numérique". Led by the artists\footnote{Comacina Capsule Creative, \url{http://www.comacina.org/}} Amandine Braconnier (mixed media artist) and Marie-Aline Villard (dancer-researcher), supported by the Fabrik Pola and the Aquitaine Région, this contemporary art project focused on the way to express emotions through robotic body movement in physical interaction with a human dancer. This work took the form of a seven day art-science residency involving members of the Poppy project and the artists. During the residency, the ease of programming through the Pypot library permitted to design a simple interface allowing the dancer to physically sculpt novel movements, which softness could be dynamically controlled. This residency took part in a French high school (Lycée Saintonge, Bordeaux) and was also an educational experiment where young students participated to workshops where they explored Poppy movements and physical interaction with the robot (see \figurename~\ref{fig:poppy_art}). The residency restitution was a contemporary art dance performance involving poetic choreography, alternating phases of autonomous robot movements and passive robot movements provoked by the dancer. A description of this experiment is available at: \url{https://forum.poppy-project.org/t/artist-residency-etres-et-numerique/72}

\begin{figure}[]
\centering
    \subfloat[][Floorwork]{\includegraphics[height=4.3cm]{art_floor.jpg}}
    \hfil
    \subfloat[][Physical interaction]{\includegraphics[height=4.3cm]{art_physical.jpg}}
    \caption{Poppy has been an actor of a contemporary dance performance involving rich physical human-robot interactions.}
    \label{fig:poppy_art}
\end{figure}
\vspace*{-5mm}
\bibliographystyle{unsrt}
\bibliography{DI2014_references}

\end{document}
